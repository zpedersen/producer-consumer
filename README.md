Zach Pedersen's Entry for Assignment 1: Producer and Consumer!
Installation Steps:

1. Install Linux VirtualBox
2. Install and configure Ubuntu virtual machine
3. Install G++ Packages on Ubuntu machine
4. Take 'main' file from Bitbucket repository
5. Create a '.c' file in Ubuntu and paste source code
6. Use terminal commands "gcc -pthread Assignment.c -o Assignment1", "./Assignment1" to compile and run